﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBEL.Demo.DAL.Entities;
using TBEL.Demo.DAL.Interfaces;

namespace TBEL.Demo.DAL.Repositories
{
    public class SongRepository : RepositoryBase<Song>, ISongRepository
    {
        public SongRepository(DemoContext context) : base(context)
        {
        }

        public IEnumerable<Song> GetByUser(int userId)
        {
            return _context.Songs.Where(s => s.UserId == userId);
        }
    }
}
