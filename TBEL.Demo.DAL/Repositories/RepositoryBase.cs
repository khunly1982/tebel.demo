﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using TBEL.Demo.DAL.Interfaces;

namespace TBEL.Demo.DAL.Repositories
{
    public abstract class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        protected readonly DemoContext _context;

        protected RepositoryBase(DemoContext context)
        {
            _context = context;
        }

        public virtual IEnumerable<TEntity> Get()
        {
            return _context.Set<TEntity>();
        }

        public virtual IEnumerable<TEntity> Find(Func<TEntity, bool> predicate)
        {
            return _context.Set<TEntity>().Where(predicate);
        }

        public virtual TEntity Find(params object[] keys)
        {
            return _context.Find<TEntity>(keys);
        }

        public virtual TEntity Add(TEntity entity)
        {
            EntityEntry<TEntity> entry = _context.Add(entity);
            entry.State = EntityState.Added;
            _context.SaveChanges();
            return entry.Entity;
        }

        public virtual bool Update(TEntity entity)
        {
            EntityEntry<TEntity> entry = _context.Entry(entity);
            entry.State = EntityState.Modified;
            return _context.SaveChanges() == 1;
        }

        public virtual bool Delete(TEntity entity)
        {
            EntityEntry<TEntity> entry = _context.Remove(entity);
            entry.State = EntityState.Deleted;
            return _context.SaveChanges() == 1;
        }
    }
}
