﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBEL.Demo.DAL.Entities;

namespace TBEL.Demo.DAL
{
    public class DemoContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Song> Songs { get; set; }

        public DemoContext(DbContextOptions options): base(options)
        {

        }
    }
}
