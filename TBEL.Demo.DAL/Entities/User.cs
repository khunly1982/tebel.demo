﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBEL.Demo.DAL.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string EncodedPassword { get; set; }
        public string Salt { get; set; }
        public string Role { get; set; }
        public string CountryIso { get; set; }
        public decimal Score { get; set; }
        public IEnumerable<Song> Songs { get; set; }
    }
}
