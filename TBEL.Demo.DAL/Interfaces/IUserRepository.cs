﻿using TBEL.Demo.DAL.Entities;

namespace TBEL.Demo.DAL.Interfaces
{
    public interface IUserRepository : IRepositoryBase<User>
    {
        User GetByEmail(string email);
    }
}