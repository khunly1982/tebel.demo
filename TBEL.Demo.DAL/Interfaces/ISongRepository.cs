﻿using System.Collections.Generic;
using TBEL.Demo.DAL.Entities;

namespace TBEL.Demo.DAL.Interfaces
{
    public interface ISongRepository : IRepositoryBase<Song>
    {
        IEnumerable<Song> GetByUser(int userId);
    }
}