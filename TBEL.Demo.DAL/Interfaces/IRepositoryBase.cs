﻿using System;
using System.Collections.Generic;

namespace TBEL.Demo.DAL.Interfaces
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        TEntity Add(TEntity entity);
        bool Delete(TEntity entity);
        TEntity Find(params object[] keys);
        IEnumerable<TEntity> Get();
        bool Update(TEntity entity);
    }
}