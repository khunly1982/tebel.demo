﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBEL.Demo.BLL.Exceptions;
using TBEL.Demo.BLL.Interfaces;
using TBEL.Demo.BLL.Models;
using TBEL.Demo.DAL.Entities;
using TBEL.Demo.DAL.Interfaces;

namespace TBEL.Demo.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IHashService _hashService;
        private readonly ICountryService _countryService;
        private readonly IUserRepository _userRepository;

        public UserService(IHashService hashService, ICountryService countryService, IUserRepository userRepository)
        {
            _hashService = hashService;
            _countryService = countryService;
            _userRepository = userRepository;
        }

        public void Register(UserModel model)
        {
            if (_userRepository.GetByEmail(model.Email) != null)
            {
                throw new DuplicateFieldException("Email");
            }
            string salt = Guid.NewGuid().ToString();
            string encodedPassword = _hashService.Hash(model.Password, salt);
            User u = new User
            {
                Email = model.Email,
                CountryIso = model.CountryIsoCode,
                EncodedPassword = encodedPassword,
                Salt = salt,
                Role= "USER",
                Score = 0,
            };
            _userRepository.Add(u);
        }

        public UserModel Login(string email, string password)
        {
            User u = _userRepository.GetByEmail(email);
            if (u != null && u.EncodedPassword == _hashService.Hash(password, u.Salt))
            {
                CountryModel c = _countryService.GetByAlpha2(u.CountryIso);
                UserModel model = new UserModel
                {
                    Id = u.Id,
                    Email = u.Email,
                    Score = u.Score,
                    CountryIsoCode = u.CountryIso,
                    Country = c,
                    Role = u.Role,
                };
                return model;
            }
            return null;
        }
    }
}
