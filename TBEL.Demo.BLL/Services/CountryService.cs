﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TBEL.Demo.BLL.Interfaces;
using TBEL.Demo.BLL.Models;

namespace TBEL.Demo.BLL.Services
{
    public class CountryService : ICountryService
    {
        private const string BASE_URL = "https://restcountries.com/v3/";

        public CountryModel GetByAlpha2(string code)
        {
            using (HttpClient client = new HttpClient())
            {
                var response = client.GetAsync(BASE_URL + "alpha/" + code).Result;
                if (response.IsSuccessStatusCode)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    IEnumerable<CountryModel> result = JsonConvert.DeserializeObject<IEnumerable<CountryModel>>(json);
                    return result.FirstOrDefault();
                }
                return null;
            }
        }

        public IEnumerable<CountryModel> GetAll()
        {
            using (HttpClient client = new HttpClient())
            {
                var response = client.GetAsync(BASE_URL + "all").Result;
                if (response.IsSuccessStatusCode)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<IEnumerable<CountryModel>>(json);
                }
                return null;
            }
        }
    }
}
