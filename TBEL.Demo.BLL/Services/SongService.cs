﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using TBEL.Demo.BLL.Interfaces;
using TBEL.Demo.BLL.Models;
using TBEL.Demo.DAL.Entities;
using TBEL.Demo.DAL.Interfaces;

namespace TBEL.Demo.BLL.Services
{
    public class SongService : ISongService
    {
        private readonly IUserRepository _userRepository;
        private readonly ISongRepository _songRepository;
        private readonly ICountryService _countryService;

        public SongService(IUserRepository userRepository, ISongRepository songRepository, ICountryService countryService)
        {
            _userRepository = userRepository;
            _songRepository = songRepository;
            _countryService = countryService;
        }

        public decimal Add(int userId, SongModel model)
        {
            User owner = _userRepository.Find(userId);
            if (owner == null) throw new Exception();

            CountryModel country = _countryService.GetByAlpha2(owner.CountryIso);
            IEnumerable<Song> songs = _songRepository.GetByUser(owner.Id);
            int lettersSum = songs.Select(s => s.Title.Length).Sum() + model.Title.Length;
            decimal newScore = lettersSum * country.Area;
            owner.Score = newScore;

            try
            {
                using TransactionScope scope = new TransactionScope();
                _userRepository.Update(owner);
                _songRepository.Add(new Song { Title = model.Title, UserId = userId });
                scope.Complete();
                return newScore;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public decimal Delete(int userid, int songId)
        {
            Song song = _songRepository.Find(songId);
            User owner = _userRepository.Find(song.UserId);
            if (owner == null || owner.Id != userid) throw new Exception();

            CountryModel country = _countryService.GetByAlpha2(owner.CountryIso);
            IEnumerable<Song> songs = _songRepository.GetByUser(owner.Id);
            int lettersSum = songs.Select(s => s.Title.Length).Sum() - song.Title.Length;
            decimal newScore = lettersSum * country.Area;
            owner.Score = newScore;

            try
            {
                using TransactionScope scope = new TransactionScope();
                _userRepository.Update(owner);
                _songRepository.Delete(song);
                scope.Complete();
                return newScore;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<SongModel> GetByUser(int userId)
        {
            return _songRepository.GetByUser(userId).Select(s => new SongModel
            {
                Id = s.Id,
                Title = s.Title,
                UserId = userId
            });
        }
    }
}
