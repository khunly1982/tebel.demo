﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBEL.Demo.BLL.Exceptions
{
    public class DuplicateFieldException : Exception
    {
        public string FieldName { get; set; }

        public DuplicateFieldException(string fieldName) : base($"The Field {fieldName} must be unique")
        {
            FieldName = fieldName;
        }
    }
}
