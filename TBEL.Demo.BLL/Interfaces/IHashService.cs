﻿namespace TBEL.Demo.BLL.Interfaces
{
    public interface IHashService
    {
        string Hash(string password, string salt = null);
    }
}