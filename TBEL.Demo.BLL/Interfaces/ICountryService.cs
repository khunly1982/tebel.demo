﻿using System.Collections.Generic;
using TBEL.Demo.BLL.Models;

namespace TBEL.Demo.BLL.Interfaces
{
    public interface ICountryService
    {
        IEnumerable<CountryModel> GetAll();
        CountryModel GetByAlpha2(string code);
    }
}