﻿using TBEL.Demo.BLL.Models;

namespace TBEL.Demo.BLL.Interfaces
{
    public interface IUserService
    {
        UserModel Login(string email, string password);
        void Register(UserModel model);
    }
}