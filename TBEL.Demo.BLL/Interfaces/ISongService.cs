﻿using System.Collections.Generic;
using TBEL.Demo.BLL.Models;

namespace TBEL.Demo.BLL.Interfaces
{
    public interface ISongService
    {
        decimal Add(int userId, SongModel model);
        decimal Delete(int userId, int songId);
        IEnumerable<SongModel> GetByUser(int userId);
    }
}