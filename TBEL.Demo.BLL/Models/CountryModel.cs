﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBEL.Demo.BLL.Models
{
    public class CountryModel
    {
        public string Alpha2Code { get; set; }
        public CountryNameModel Name { get; set; }
        public decimal Area { get; set; }
    }

    public class CountryNameModel
    {
        public string Common { get; set; }
    }
}
