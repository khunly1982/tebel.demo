﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.Security.Models;

namespace TBEL.Demo.BLL.Models
{
    public class UserModel: IPayload
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string CountryIsoCode { get; set; }
        public string Role { get; set; }
        public decimal Score { get; set; }
        public CountryModel Country { get; set; }
        public string Identifier 
        { 
            get { return Id.ToString(); } 
        }
        public IEnumerable<string> Roles 
        { 
            get { yield return Role; } 
        }
    }
}
