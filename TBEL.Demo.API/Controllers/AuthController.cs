﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TBEL.Demo.API.DTOS.User;
using TBEL.Demo.API.DTOS.User.Forms;
using TBEL.Demo.BLL.Exceptions;
using TBEL.Demo.BLL.Interfaces;
using TBEL.Demo.BLL.Models;
using ToolBox.Security.Services;

namespace TBEL.Demo.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly JwtService _jwtService;

        public AuthController(IUserService userService, JwtService jwtService)
        {
            _userService = userService;
            _jwtService = jwtService;
        }

        [HttpPost("Login")]
        [Produces(typeof(UserDetailsDTO))]
        public IActionResult Login(LoginFormDTO dto)
        {
            try
            {
                UserModel model = _userService.Login(dto.Email, dto.Password);
                if (model == null) return Unauthorized();
                else return Ok(new UserDetailsDTO { 
                    Id = model.Id,
                    Email = model.Email,
                    Score = model.Score,
                    CountryIsoCode = model.CountryIsoCode,
                    CountryName = model.Country.Name.Common,
                    Token = _jwtService.CreateToken(model),
                });
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPost("Register")]
        public IActionResult Register(RegisterFormDTO dto)
        {
            try
            {
                _userService.Register(new UserModel { 
                    Email = dto.Email,
                    Password = dto.Password,
                    CountryIsoCode = dto.CountryIsoCode
                });
                return NoContent();
            }
            catch (DuplicateFieldException ex)
            {
                ModelState.AddModelError(ex.FieldName, ex.Message);
                return BadRequest();
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
