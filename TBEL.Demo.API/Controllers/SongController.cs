﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TBEL.Demo.API.Core;
using TBEL.Demo.API.DTOS.Song;
using TBEL.Demo.API.DTOS.Song.Forms;
using TBEL.Demo.API.Utils;
using TBEL.Demo.BLL.Interfaces;
using TBEL.Demo.BLL.Models;

namespace TBEL.Demo.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiAuthorization("USER")]
    public class SongController : ControllerBase
    {
        private readonly ISongService _songService;

        public SongController(ISongService songService)
        {
            _songService = songService;
        }

        [HttpGet]
        [Produces(typeof(IEnumerable<SongDTO>))]
        public ActionResult Index()
        {
            try
            {
                return Ok(_songService.GetByUser(this.GetUserId()).Select(s => new SongDTO { 
                    Id = s.Id,
                    Title = s.Title,
                    UserId = s.UserId
                }));
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPost]
        [Produces(typeof(decimal))]
        public ActionResult Create(SongFormDTO dto)
        {
            try
            {
                return Ok(_songService.Add(this.GetUserId(), new SongModel {
                    Title = dto.Title,
                }));
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpDelete("{songId}")]
        [Produces(typeof(decimal))]
        public ActionResult Delete(int songId)
        {
            try
            {
                return Ok(_songService.Delete(this.GetUserId(), songId));
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
