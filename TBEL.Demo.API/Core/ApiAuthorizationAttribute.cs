﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TBEL.Demo.API.Core
{
    public class ApiAuthorizationAttribute : Attribute, IAuthorizationFilter
    {
        private string[] _roles { get; set; }
        public ApiAuthorizationAttribute(params string[] roles)
        {
            _roles = roles;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            foreach(string role in _roles)
            {
                if(context.HttpContext.User.IsInRole(role))
                {
                    return;
                }
            }
            context.Result = new UnauthorizedResult();
        }
    }
}
