﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TBEL.Demo.API.DTOS.Song.Forms
{
    public class SongFormDTO
    {
        [Required]
        [MaxLength(255)]
        public string Title { get; set; }
    }
}
