﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TBEL.Demo.API.DTOS.User
{
    public class UserDetailsDTO
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string CountryIsoCode { get; set; }
        public string CountryName { get; set; }
        public decimal Score { get; set; }
        public string Token { get; set; }
    }
}
