﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TBEL.Demo.API.DTOS.User.Forms
{
    public class RegisterFormDTO
    {
        [Required]
        [EmailAddress]
        [MaxLength(255)]
        public string Email { get; set; }

        [Required]
        [MaxLength(255)]
        public string Password { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(2)]
        public string CountryIsoCode { get; set; }
    }
}
